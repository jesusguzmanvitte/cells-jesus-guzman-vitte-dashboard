{
  const {
    html,
  } = Polymer;
  /**
    `<cells-jesus-guzman-vitte-dashboard>` Description.

    Example:

    ```html
    <cells-jesus-guzman-vitte-dashboard></cells-jesus-guzman-vitte-dashboard>
    ```

    ## Styling
    The following custom properties and mixins are available for styling:

    ### Custom Properties
    | Custom Property     | Selector | CSS Property | Value       |
    | ------------------- | -------- | ------------ | ----------- |
    | --cells-fontDefault | :host    | font-family  |  sans-serif |
    ### @apply
    | Mixins    | Selector | Value |
    | --------- | -------- | ----- |
    | --cells-jesus-guzman-vitte-dashboard | :host    | {} |

    * @customElement
    * @polymer
    * @extends {Polymer.Element}
    * @demo demo/index.html
  */
  class CellsJesusGuzmanVitteDashboard extends Polymer.mixinBehaviors([ CellsBehaviors.i18nBehavior, ], Polymer.Element) {
    static get is() {
      return 'cells-jesus-guzman-vitte-dashboard';
    }

    static get properties() {
      return {
        filter: {
          type: String,
          value: 'Filtro'
        },
        filtervalue: {
          type: String,
          value: '',
        },
        domif: {
          type: Boolean,
          value: false
        },
        students: {
          type: Object,
          value: {}
        },
        users: {
          type: Object,
          value: {}
        }
      };
    }

    static get template() {
      return html `
      <style include="cells-jesus-guzman-vitte-dashboard-styles cells-jesus-guzman-vitte-dashboard-shared-styles"></style>
      <slot></slot>
      <template is="dom-if" if="[[domif]]">
        <p>
          <cells-molecule-input name="filter" label="Filter" on-keyup="reFilter" type="text" value="{{filtervalue::input}}">
          </cells-molecule-input>
        </p>
        <template id="tmpStudents" is="dom-repeat" items="{{students}}" filter="filterByFirst">
          <cells-business-contact-card id="demo{{index}}" card-name="{{item.name}}&nbsp;{{item.last}}" card-title="{{item.address}}" card-image="{{item.img}}" card-title-icon="{{coronita:help}}">
          </cells-business-contact-card>
        </template>
      </template>
      <template is="dom-if" if="[[!domif]]">
        <cells-spinner-stripes id="spinner"></cells-spinner-stripes>
      </template>
      `;
    }

    ready() {
      super.ready();
      console.log(window.AppConfig.testGlobalValue.value);
      //Polymer.dom(this.root).querySelector('#allCards').style.display = 'allCards';
    }

    filterByFirst(item) {
      return item.name.toLowerCase().indexOf(this.filtervalue.toLowerCase()) !== -1 || item.last.toLowerCase().indexOf(this.filtervalue.toLowerCase()) !== -1;
    }
    loadData(e) {
      console.log(e);
      this.students=e.students;
      this.domif=true;
      Polymer.dom(this.root).querySelector('#tmpStudents').render();
    }

    reFilter() {
      Polymer.dom(this.root).querySelector("#tmpStudents").render();
    }

  }

  customElements.define(CellsJesusGuzmanVitteDashboard.is, CellsJesusGuzmanVitteDashboard);
}